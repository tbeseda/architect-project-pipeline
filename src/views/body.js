module.exports = /*html*/`
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Architect</title>
  <style>
    * { margin: 0; padding: 0; box-sizing: border-box; } body { max-width: 20rem; margin: 2rem auto; font-family: -apple-system, sans-serif; }
  </style>
</head>
<body>
  <div>
    <img src="https://assets.arc.codes/logo.svg" />
    <div>
      <div>
        <h1>
          Hello from an Architect Node.js function!
        </h1>
        <p>
          Deployed automatically <a href="https://gitlab.com/tbeseda/architect-project-pipeline">from GitLab</a> 💪
        </p>
      </div>
      <div>
        <p>
          View documentation at:
          <code>
            <a href="https://arc.codes">https://arc.codes</a>
          </code>
        </p>
      </div>
    </div>
  </div>
</body>
</html>
`
