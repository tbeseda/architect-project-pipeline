const body = require('@architect/views/body.js')
const headers = require('@architect/views/headers.js')

exports.handler = async function http () {
  return {
    statusCode: 200,
    headers,
    body,
  }
}
